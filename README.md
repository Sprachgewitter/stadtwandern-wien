# Stadtwandern Wien - Cordova Mobile App

This mobile hiking application for the citytrails of Vienna ("Stadtwanderwege Wien") is based on **Apache Cordova** framework which allows you to develop for multiple platforms (especially Android and iOS) at the same time.

It has been developed as final project for my training as JavaScript software developer. In January 2022, it has been published in Google Play: https://play.google.com/store/apps/details?id=at.sprachgewitter.wanderapp

The app uses data provided by the City of Vienna (https://data.wien.gv.at).

## Build

Clone the application.

Install **Apache Cordova v10.1.0**.

`npm install cordova@10.1.0`

Then install the platforms on which you want to use the application. For example:

`cordova platform add android`

or

`cordova platform add ios`

## Run the application

You can run the application with the command "cordova run + your chosen platform", for example:

`cordova run android`

## Plugins

The following plugins have been used in this application (installed automatically):
- cordova-plugin-file
- cordova-plugin-file-transfer
- cordova-plugin-foreground-service
- cordova-plugin-background-mode
- cordova-plugin-geolocation
