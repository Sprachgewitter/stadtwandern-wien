/** Create view for picture gallery (all trails)
 * 
 * @param {object[]} pictures [path, name of trail]
 */
const createSlides = (pictures) => {
    pictures.forEach(pic => {

        if (typeof(pic[0]) !== 'undefined' && typeof(pic[1]) !== 'undefined') {
            let trackID = pic[1].slice(15, pic[1].length);

            let a = 
                $('<a>')
                    .attr('href', APP_URL + '/details.html?track=' + trackID)
                    .appendTo('div.gallery-container');
            let div = 
                $('<div>')
                    .addClass('mySlides')
                    .appendTo(a);
            let img = 
                $('<img>')
                    .attr('src', SERVER_URL + '/' + pic[0])
                    .appendTo(div);
            $('<figcaption>')
                .addClass('figcaption')
                .text(pic[1])
                .insertAfter(img);
        }
    });
};

/** Request pictures of all trails for gallery from server */
const getAllPictures = () => {
    $.ajax({
        url: SERVER_URL + '/members/getallpictures',
        method: 'GET',
        xhrFields: { withCredentials: true },
        success: (response) => {
            createSlides(response);
        },
        error: (xhr, status, errorText) => {
            console.log('HTTP_Fehler, Status:', status, errorText);
        }
    });
};

getAllPictures();