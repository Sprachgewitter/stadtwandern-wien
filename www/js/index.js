/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/**********   CORDOVA Initialization   **********/

document.addEventListener('deviceready', onDeviceReady, false);

function onDeviceReady() {

    console.log('Running cordova-' + cordova.platformId + '@' + cordova.version);
    document.getElementById('deviceready').classList.add('ready');
};

document.addEventListener('DOMContentLoaded', () => {
    init();
});

/**********  END CORDOVA Initialization   **********/

const btnLogin = $('#btn-login');
const linkLogin = $('#link-login');
const linkForgotPW = $('#link-forgot-pw');
const linkRegister = $('#link-register');
const linkDeleteAccount = $('#link-delete-account');
const btnDeleteAccount = $('#btn-delete-account');
const btnGetNewPW = $('#btn-get-new-pw');
const btnSaveNewPW = $('#btn-save-new-pw');
const formRegister = $('#form-register');
const formDeleteAccount = $('#form-delete-account');

/** Add event listener to login button */
const loginEvent = () => {
    if (btnLogin !== null) {
        btnLogin.on('click', e => {
            e.preventDefault();
            login();
        });
    }
};

let username = '';

/** Send login request to server */
const login = () => {
    username = $('#form-login input[name="username"]').val();
    $.ajax({
        url: SERVER_URL + '/login',
        method: 'POST',
        data: {
            username: username,
            password: $('#form-login input[name="password"]').val()
        },
        xhrFields: { withCredentials: true },
        success: (response) => {
            if (response === "Login OK") {
                window.location.assign('alltrails.html');
            } else {
                showLoginFail();
            }
        },
        error: (xhr, status, errorText) => {
            console.log("HTTP-Fehler, Status: ", status, errorText);
            showLoginFail();
        }
    });
};

/** Show user notification (login fail) */
const showLoginFail = () => {
    createModalBox($('#main-login'));
    createModalContent('Login fehlgeschlagen', 'Login-Daten nicht korrekt oder E-Mail-Adresse noch nicht bestätigt.');
};

/** Show form for new password request */
const showForgotPW = () => {
    if (linkForgotPW !== null) {
        linkForgotPW.on('click', (e) => {
            e.preventDefault();
            $('#form-forgot-pw').removeClass('hidden');
            $('#form-register').addClass('hidden');
            $('#link-forgot-pw').text('Neues Passwort anfordern');
            $('#link-register').text('Noch keinen User Account?');
            $('#form-login').addClass('hidden');
            $('#div-delete-account').addClass('hidden');
            linkForgotPW.css('font-size', '1.4rem');
            linkLogin.css('font-size', '1.1rem');
            linkDeleteAccount.css('font-size', '1.1rem');
            linkRegister.css('font-size', '1.1rem');
            linkDeleteAccount.css('font-size', '1.1rem');
            $('#main-div-login').prepend($('#section-password'));

        });
    }
};

/** Show form for user registration */
const showRegister = () => {
    if (linkRegister !== null) {
        linkRegister.on('click', (e) => {
            e.preventDefault();
            $('#form-register').removeClass('hidden');
            $('#form-forgot-pw').addClass('hidden');
            $('#link-register').text('Neuen Account anlegen')
            $('#link-forgot-pw').text('Passwort vergessen?');
            $('#form-login').addClass('hidden');
            $('#div-delete-account').addClass('hidden');
            linkForgotPW.css('font-size', '1.1rem');
            linkDeleteAccount.css('font-size', '1.1rem');
            linkLogin.css('font-size', '1.1rem');
            linkDeleteAccount.css('font-size', '1.1rem');
            linkRegister.css('font-size', '1.4rem');
            $('#main-div-login').prepend($('#section-register'));

        });
    };
}

const showDeleteAccount = () => {
    if (linkDeleteAccount !== null) {
        linkDeleteAccount.on('click', (e) => {
            e.preventDefault();
            $('#div-delete-account').removeClass('hidden');
            $('#form-forgot-pw').addClass('hidden');
            $('#form-login').addClass('hidden');
            $('#form-register').addClass('hidden');
            linkDeleteAccount.css('font-size', '1.4rem');
            linkForgotPW.css('font-size', '1.1rem');
            linkLogin.css('font-size', '1.1rem');
            linkRegister.css('font-size', '1.1rem');
            $('#main-div-login').prepend($('#section-delete'));
        })
    }
};

const deleteAccountEvent = () => {
    if (btnDeleteAccount !== null) {
        btnDeleteAccount.on('click', (e) => {
            e.preventDefault();
            $('#form-forgot-pw').addClass('hidden');
            $('#form-login').addClass('hidden');
            $('#form-register').addClass('hidden');
            deleteAccount();
        });
    };
};

/** Show form user login */
const showLogin = () => {
    if (linkLogin !== null) {
        linkLogin.on('click', (e) => {
            e.preventDefault();
            $('#form-login').removeClass('hidden');
            $('#form-register').addClass('hidden');
            $('#link-register').text('Noch keinen Account?');
            $('#form-forgot-pw').addClass('hidden');
            $('#div-delete-account').addClass('hidden');
            linkForgotPW.css('font-size', '1.1rem');
            linkLogin.css('font-size', '1.4rem');
            linkDeleteAccount.css('font-size', '1.1rem');
            linkRegister.css('font-size', '1.1rem');
            $('#main-div-login').prepend($('#section-login'));
        });
    }
};

/** Add event listener on forgot password button */
const getNewPwEvent = () => {
    if (btnGetNewPW !== null) {
        btnGetNewPW.on('click', (e) => {
            e.preventDefault();
            getNewPw();
        });
    }
};

/** Validate user input
 * 
 * @param {string} name min. 4 alphanumeric characters
 * @param {string} email min. 7 alphanumeric characters
 * @returns 
 */
const checkInput2 = (name, email) => {
    if (name.length >= 4 &&
        regexAlphaNum.test(name) &&
        email.length >= 7) {
        return true;
    } else {
        return false;
    }
}

/** Send request for new password to server */
const getNewPw = () => {
    let username = $('#form-forgot-pw input[name="username"]').val().trim();
    let email = $('#form-forgot-pw input[name="email"]').val().trim();

    let validation = checkInput2(username, email);
    if (validation) {
        $.ajax({
            url: SERVER_URL + '/forgotpw',
            method: 'POST',
            data: {
                username: username,
                email: email
            },
            xhrFields: { withCredentials: true },
            success: (response) => {
                showPleaseConfirm();
            },
            error: (xhr, status, errorText) => {
                console.log("HTTP-Fehler, Status: ", status, errorText);
                showServerError();
            }
        })
    } else {
        showInputFail();
    }
};

/** Send request to delete account to server */
const deleteAccount = () => {
    let username = $('#form-delete-account input[name="username"]').val().trim();
    let email = $('#form-delete-account input[name="email"]').val().trim();

    let validation = checkInput2(username, email);

    if (validation) {
        $.ajax({
            url: SERVER_URL + '/deletionrequest',
            method: 'POST',
            data: {
                username: username,
                email: email
            },
            xhrFields: { withCredentials: true },
            success: (response) => {
                showPleaseConfirmDeletion();
            },
            error: (xhr, status, errorText) => {
                console.log("HTTP-Fehler, Status: ", status, errorText);
                showServerError();
            }
        })
    } else {
        showInputFail();
    }
};

/** Show user notification (invalid user input) */
const showInputFail = () => {
    createModalBox($('#main-login'));
    createModalContent('Ungültige Eingabe', 'Bitte gib einen gültigen Usernamen' +
        ' und eine gültige E-Mail-Adresse ein.');
};

/** Show user notfication (confirm email address for registration) */
const showPleaseConfirm = () => {
    createModalBox($('#main-login'));
    createModalContent('Fast geschafft!', `Es wurde ein E-Mail an die angegebene Adresse gesendet.
    Bitte klick auf den enthaltenen Link zur Bestätigung deiner E-Mail-Adresse.`);
};

/** Show user notification of http error (server error) */
const showServerError = () => {
    createModalBox($('#main-login'));
    createModalContent('Ups ...', 'Leider ist etwas schief gegangen.' +
        'Bitte versuch es in ein paar Minuten erneut.');
};

/** Show user notification (confirm email address for account deletion) */
const showPleaseConfirmDeletion = () => {
    createModalBox($('#main-login'));
    createModalContent('Fast geschafft!', `Um zu verhindern, dass eine unbefugte Person deinen Account löscht,
    haben wir ein E-Mail an die angegebene Adresse gesendet. Bitte klick auf den enthaltenen Link, um das Löschen
    deines Accounts zu bestätigen.`);
};

/** Initialization of event listeners after DOMContentLoaded event */
const init = () => {
    loginEvent();
    showForgotPW();
    showRegister();
    getNewPwEvent();
    showLogin();
    showDeleteAccount();
    deleteAccountEvent();
};
