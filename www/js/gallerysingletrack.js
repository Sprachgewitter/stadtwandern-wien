/** Create view for picture gallery (selected trail)
 * 
 * @param {object[]} pictures [path, name of trail]
 */
const createSlides = (pictures) => {

    let index = 0;
    let headerdiv = $('<div>').addClass('mySlides').appendTo('#selected-gallery>div.gallery-container');
    $('<h2>').text(pictures[0][1]).appendTo(headerdiv);

    pictures.forEach(pic => {
        let div = $('<div>').addClass('mySlides').insertAfter(headerdiv);
        let img = $('<img>').attr('src', SERVER_URL + '/' + pic[0]).appendTo(div);
    });
};

/** Request pictures of selected trail for gallery from server
 * 
 * @param {string} trackID ID of selected trail
 */
const getPicturesForSelectedTrack = (trackID) => {
    $.ajax({
        url: SERVER_URL + '/members/getpictures/' + trackID,
        method: 'GET',
        xhrFields: { withCredentials: true },
        success: (response) => {
            createSlides(JSON.parse(response));
        },
        error: (xhr, status, errorText) => {
            console.log('HTTP_Fehler, Status:', status, errorText);
        }
    });
};

const urlParams = new URLSearchParams(window.location.search);
const trackID = urlParams.get('track');

getPicturesForSelectedTrack(trackID);