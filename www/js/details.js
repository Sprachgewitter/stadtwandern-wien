
/** Get trails from local storage */
let allTrailsLocalStorage = JSON.parse(localStorage.getItem('allTrailsLocalStorage'));

if (allTrailsLocalStorage) {
    console.log('Wege aus dem LocalStorage geladen: ', allTrailsLocalStorage);
} else {
    console.log('Keine Daten aus dem Localstorage verfügbar.');
    createModalBox($('#traildetail'));
    createModalContent('Ups ...', 'Leider ist ein Fehler aufgetreten. Bitte starte die App neu.');
}

/** Create a map, located at Stephansplatz, zoom factor 13 */
let map = L.map('map').setView([48.208275920803175, 16.373201337869936], 13);
let layer = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 20,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'YOUR ACCESSTOKEN'
}).addTo(map);

/** Locate current user position */
map.locate();

let markerCurrentPosition;
let circleCurrentPosition;

/** Bind popup to current user position */
map.once('locationfound', e => {
    markerCurrentPosition = L.marker(e.latlng).addTo(map)
        .bindPopup('<p>Du bist hier!</p>');
});

/** Reverse lat/lng coordinates
 * 
 * @param {object[]} coordinates array with numbers
 * @returns array with reversed lat/lng coordinates
 */
const reverseLatLng = (coordinates) => {
    coordinates.forEach(point => {
        point.reverse();
    });
    return coordinates;
}

let selTrailID;
let selTrailCoordinates;
let selTrailName;
let selTrailDuration;
let selTrailDistance;
let selTrailStartCoordinates;
let selTrailStart;
let selTrailRoute;
let selTrailCatering;

const btnBeenThere = $('#btn-been-there');

/** Get data for selected trail */
const getSelectedTrail = () => {

    /** Get trackID from querystring */
    const urlSearchParams = new URLSearchParams(window.location.search);
    let trackID = urlSearchParams.get('track');

    /** Cash data for selected trail */
    allTrailsLocalStorage.forEach(e => {

        let x = e.name.slice(15, e.name.indexOf(':'));
        if (x === trackID) {
            selTrailID = trackID;
            selTrailCoordinates = reverseLatLng(e.coordinates);
            selTrailName = e.name;
            selTrailDuration = e.duration;
            selTrailDistance = e.distance;
            selTrailStartCoordinates = e.startCoordinates;
            selTrailStart = e.start;
            selTrailRoute = e.route;
            selTrailCatering = e.catering;
        }
    });
};

/** Show selected trail on map */
const showTrail = () => {

    getSelectedTrail();
    selTrailCoordinates = selTrailCoordinates.reverse();

    /** Draw trail in map */
    let polyline = L.polyline(selTrailCoordinates, { color: '#009900' }).addTo(map);
    map.fitBounds(polyline.getBounds());

    /** Bind popup to starting point of trail */
    L.marker(selTrailStartCoordinates).addTo(map)
        .bindPopup(`<p>Start Stadtwanderweg ${selTrailID}</p>`)
        .openPopup();
};

/** Create view for details.html */
const showInfo = () => {

    let row1 =
        $('<div>')
            .addClass('row')
            .attr('id', 'row-titles')
            .appendTo('#details-page>div');
    let iconTrailNr =
        $('<div>')
            .text(selTrailID)
            .addClass('icon-trail-nr')
            .appendTo(row1);
    $('<h2>')
        .text(selTrailName.slice(selTrailName.indexOf(':') + 1, selTrailName.length))
        .insertAfter(iconTrailNr);

    let row2 =
        $('<div>')
            .addClass('row')
            .attr('id', 'row-map')
            .insertAfter(row1);
    $('div#map').appendTo(row2);
    let row3 =
        $('<div>')
            .addClass('row')
            .insertAfter(row2);
    let info1 = $(`
    <div>
        <h3>Allgemeine Infos</h3>
        <p>Länge: ${selTrailDistance}</p>
        <p>Gehzeit: ${selTrailDuration}</p>
        <p>Startpunkt: ${selTrailStart}</p>

        <h3>Route</h3>
        <p>${selTrailRoute}</p></div>
    `).appendTo(row3);

    let row4 =
        $('<div>')
            .addClass('row')
            .insertAfter(row3);
    let h2 = 
        $('<h3>')
            .text('Einkehrmöglichkeiten')
            .appendTo(row4);
    let row5 = 
        $('<div>')
            .addClass('row')
            .insertAfter(row4);
    let catering = 
        $('<ul>')
            .appendTo(row5);
    selTrailCatering.forEach(e => {
        $('<li>')
            .text(e)
            .appendTo(catering);
    });
    let row6 = 
        $('<div>')
            .addClass('row')
            .insertAfter(row5);
    $('button#btn-start-tracking').appendTo(row6);
    let row7 = 
        $('<div>')
            .addClass('row')
            .insertAfter(row6);
    btnBeenThere.appendTo(row7);

    navLinkGallery();
}

/** Create btn to picture gallery of currently selected trail */
const navLinkGallery = () => {
    const urlParams = new URLSearchParams(window.location.search);
    const trackID = urlParams.get('track');
    let btnShowPics = $('#nav-show-sel-pic');
    btnShowPics.attr('href', `gallerysingletrack.html?track=${trackID}`);
};

showTrail();
showInfo();

/*****************   TRACKING   *************************/

/** Switch view to big map and no details (for tracking) */
const showBigMap = () => {
    $('#details-page .row').css('display', 'none');
    $('#row-titles').css('display', 'block');
    $('#row-map').css('display', 'block');
    $('#map').css('height', '73vh');
    $('#row-take-pic').css('display', 'block');
};

/** Switch view back from big map to normal view with details */
const showNormalMap = () => {
    $('#details-page .row').css('display', 'block');
    $('#btn-stop-tracking').css('display', 'none');
    $('#map').css('height', '400px');
};

/** Remove marker from previous user position */
const removeMarker = () => {
    if (markerCurrentPosition) {
        // remove marker of initial position
        map.removeLayer(markerCurrentPosition);
    }
    if (circleCurrentPosition) {
        // remove circle of previous position
        map.removeLayer(circleCurrentPosition);
    }
}

/** Show circle at current user position */
const circlePosition = (e) => {
    circleCurrentPosition = L.circle(e, {
        color: '#5074eb',
        fillColor: '#5074eb',
        fillOpacity: .2,
        radius: 15
    }).addTo(map);
}

const btnStartTracking = $('#btn-start-tracking');

btnStartTracking.on('click', (e) => {
    e.preventDefault();
    startTracking();
});

/** Start tracking and draw user trail in map */
const startTracking = () => {
    showBigMap();
    
    cordova.plugins.foregroundService.start('GPS running', 'Background Service');      
    cordova.plugins.backgroundMode.enable();
    cordova.plugins.backgroundMode.on('activate', function () {
        console.log('Backgroundmode is active...');
        cordova.plugins.backgroundMode.moveToForeground();
        cordova.plugins.backgroundMode.disableWebViewOptimizations(); 
        cordova.plugins.backgroundMode.wakeUp();
        console.log('App wieder im Vordergrund: ', cordova.plugins.backgroundMode.isActive());
    });

    map.locate({ watch: true, enableHighAccuracy: true });
    map.on('locationfound', handleLocationFound);
    btnStopTracking();
    polylineTracking = L.polyline(trackedUserTrail, { color: '#5074eb' });
    polylineTracking.addTo(map);

};

// parameters for accuray in meters
const initialMinAccuracy = 3;
const maxInaccurateLocationThreshold = 5;
let minAccuracy = initialMinAccuracy;
let inaccurateLocationCounter = 0;

let polylineTracking;
let trackedUserTrail = [];
let countLocFound = 10;

/** Dynamically define min/max accuracy for postition,
 * according to quality (=accuracy) of received geodata */
const handleLocationFound = (e) => {
    removeMarker();
    console.log('Current user position: ', e.latlng, 'Accuracy: ', e.accuracy);
    // Ignore coordinates with low accuracy!
    if (e.accuracy <= minAccuracy) {
        trackedUserTrail.push([e.latlng.lat, e.latlng.lng]);
        polylineTracking.addLatLng([e.latlng.lat, e.latlng.lng]);
        console.log('Accuracy OK. Coordinates saved.');
        // Resets minAccuracy to initial value as soon as an accurate position is available
        if (e.accuracy <= initialMinAccuracy) {
            minAccuracy = initialMinAccuracy;
        }
    } else {
        // Increase counter if no accurate position is available
        inaccurateLocationCounter++;
        console.log('Accuracy NOK. Coordinates NOT saved.');
        // Increase accuracy if we get inaccurate position too often
        if (inaccurateLocationCounter > maxInaccurateLocationThreshold) {
            minAccuracy++; // increase minAccuracy to get coordinates
        }
    }

    circlePosition(e.latlng);

    // Finds out if user is off track and informs user
    if (countLocFound >= 10) {
        calcDistance(e.latlng.lat, e.latlng.lng);
        countLocFound = 0;
    };
    countLocFound++;
};


/** Create btn for stopping the tracking */
const btnStopTracking = () => {
    let btn = 
        $('<button>')
            .addClass('btn-large')
            .attr('id', 'btn-stop-tracking')
            .text('Wanderung beenden')
            .insertAfter('#map');
    btn.on('click', (e) => {
        e.preventDefault();
        stopTracking();
    });
};

/** Get trackID from querystring
 * 
 * @returns trackID (as string)
 */
const getTrackIDFromURL = () => {
    const urlSearchParams = new URLSearchParams(window.location.search);
    return urlSearchParams.get('track');
};

/** Stop tracking */
const stopTracking = () => {
    map.locate({ watch: false });
    map.off('locationfound', handleLocationFound); // unregisters locationfound event

    showNormalMap();
    map.locate({ watch: false });

    cordova.plugins.foregroundService.stop();
    cordova.plugins.backgroundMode.disable();
    console.log('BackgroundMode stopped');

    // Only send trail to server, if it contains at least 1 pair of coordinates
    if (trackedUserTrail.length > 1 && trackedUserTrail.length < 50) {
        let msg = `Es wurden nur ${trackedUserTrail.length} Koordinaten aufgezeichnet.
        Möchtest du die Route trotzdem speichern?`
        showSaveOrDelete(msg);
    } else if (trackedUserTrail.length >= 50 ) {
        let msg = 'Möchtest du die Route speichern?';
        showSaveOrDelete(msg);
    } else {
        trackedUserTrail = [];
        showNoTrackingData();
    }
};

const sendTrackedTrail = () => {
    let trackID = getTrackIDFromURL();
    let currentTrail = JSON.stringify(trackedUserTrail);

    $.ajax({
        url: SERVER_URL + '/members/savetrail',
        method: 'POST',
        data: {
            coordinates: currentTrail,
            trackID: trackID
        },
        xhrFields: { withCredentials: true },
        success: (response) => {
            trackedUserTrail = [];
            showTrailSaveSuccess();
        },
        error: (xhr, status, errorText) => {
            console.log('HTTP-Fehler, Status:', status, errorText);
            showServerError();
        }
    });
};

/** Add trail to list "done", needed for view "done trails" */
const markTrailAsDone = () => {
    let trackID = getTrackIDFromURL();
    $.ajax({
        url: SERVER_URL + '/members/markasdone',
        method: 'POST',
        data: { track: trackID },
        xhrFields: { withCredentials: true },
        success: (response) => {
            showMarkedAsDone(trackID);
        },
        error: (status, errorText) => {
            console.log('Http-Fehler, Status: ', status, errorText);
        }
    });
};

const btnMarkTrailAsDone = $('#btn-been-there');
btnMarkTrailAsDone.on('click', e => {
    e.preventDefault();
    markTrailAsDone();
});

/** Show user notification (trail marked as done) */
const showMarkedAsDone = (trackID) => {
    createModalBox($('#details-page'));
    createModalContent('Alles klar!', `Stadtwanderweg ${trackID} wurde als gemacht gespeichert.`);
    $('#row-map').css('display', 'none');
};

/** Show user notification (trail saved) */
const showTrailSaveSuccess = () => {
    createModalBox($('#details-page'));
    createModalContent('Route gespeichert!', 'Unter "Meine Routen" kannst du deine aufgezeichneten Wege sehen.');
    $('#row-map').css('display', 'none');
};

/** Show user notification of http error (server error) */
const showServerError = () => {
    createModalBox($('#details-page'));
    createModalContent('Ups ...', 'Leider ist etwas schief gegangen.' +
        'Bitte versuch es in ein paar Minuten erneut.');
    $('#row-map').css('display', 'none');
}

/** Show user notifiction of http error (server error) */
const showNoTrackingData = () => {
    createModalBox($('#details-page'));
    createModalContent('Ups ...',
        'Es wurden keine Trackingdaten aufgezeichnet.');
    $('#row-map').css('display', 'none');
    polylineTracking.remove(map);
}

/** Show user notification if less than 10 coordinates are to be saved */
const showSaveOrDelete = (message) => {
    createModalBox($('#details-page'));
    createModalContent(message);
    createButton($('div.modal-content'), 'Ja', 'yes');
    createButton($('div.modal-content'), 'Nein', 'no');
    $('#row-map').css('display', 'none');

    $('button.btn-modal-yes').on('click', (e) => {
        e.preventDefault();
        sendTrackedTrail();
        $('div.modal').css('display', 'none');
        if ($('#row-map')) {
            $('#row-map').css('display', 'block');
        }
        polylineTracking.remove(map);

    });
    $('button.btn-modal-no').on('click', (e) => {
        e.preventDefault();
        $('div.modal').css('display', 'none');
        if ($('#row-map')) {
            $('#row-map').css('display', 'block');
        }
        trackedUserTrail = [];
        polylineTracking.remove(map);
    });

};

const createButton = (selector, text, cssClass) => {
    $('<button>')
    .attr('type', 'button')
    .addClass(`btn-modal-${cssClass}`)
    .text(text)
    .appendTo(selector);
};

/** Calculate distance between user and selected track */
const calcDistance = (lat1, lng1) => {
    let p = 0.017453292519943295; // Math.PI / 180
    let c = Math.cos;
    let distances = [];

    selTrailCoordinates.forEach(point => {
        let lat2 = point[0];
        let lng2 = point[1];
        let a = 0.5 - c((lat2 - lat1) * p) / 2 +
            c(lat1 * p) * c(lat2 * p) *
            (1 - c((lng2 - lng1) * p)) / 2;
        let distance = 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
        distances.push(distance);
    })
    let onTrack = false;
    // User is off track if distance to trail > 500m
    distances.forEach(e => {
        if (e < 0.5) {
            onTrack = true;
        }
    });
    if (!onTrack) {
        markerCurrentPosition = L.marker({ lat: lat1, lng: lng1 }).addTo(map)
            .bindPopup('<p>Achtung! Du befindest dich nicht auf der Route.</p>')
            .openPopup();
    }
};
