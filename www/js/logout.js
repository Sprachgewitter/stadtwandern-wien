$(document).ready(() => logoutEvent());

/** Add event listener on logout button */
const logoutEvent = () => {
    const btn = document.getElementById('btn-logout');
    btn.addEventListener('click', e => {
        e.preventDefault();

        $.ajax({
            url: SERVER_URL + '/members/logout',
            method: 'GET',
            xhrFields: { withCredentials: true },
            success: (response) => {
                window.location.assign('index.html');
            },
            error: (xhr, status, errorText) => {
                console.log('HTTP-Fehler, Status: ', status, errorText);
            }
        });
    });
};