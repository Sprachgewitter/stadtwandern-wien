const SERVER_URL = 'https://wanderwege.catonauts.com';
const APP_URL = '';

const regexAlphaNum = /^[a-z0-9]+$/i;
const stadtwanderwege = ['1', '1a', '2', '3', '4', '4a','5', '6', '7', '8', '9', '10', '11', '12'];

/** Create a modal box for user notification
 * 
 * @param {selector} selector selector for html element where modal box shall be inserted
 */
const createModalBox = (selector) => {
    let modalContainer = 
        $('<div>')
            .addClass('modal')
            .appendTo(selector);

    let modalBox = 
        $('<div>')
            .addClass('modal-content')
            .appendTo(modalContainer);

    $('<button>')
        .attr('type', 'button')
        .addClass('btn-modal-close')
        .text('x')
        .appendTo(modalBox);

    $('<h3>').appendTo(modalBox);
    let p = $('<p>').appendTo(modalBox);

    modalContainer.css('display', 'block');

    $('button.btn-modal-close').on('click', (e) => {
        e.preventDefault();
        modalContainer.css('display', 'none');
        if ($('#row-map')) {
            $('#row-map').css('display', 'block');
        }
    });
};

/** Add content to modal box (created via createModalBox())
 * 
 * @param {string} title message title
 * @param {string} text message
 */
const createModalContent = (title, text) => {
    $('div.modal h3').text(title);
    $('div.modal p').text(text);
}

const btnGoBack = $('#btn-back');

btnGoBack.on('click', e => {
    e.preventDefault;
    history.back();
});