document.addEventListener('DOMContentLoaded', (event) => {
    init2();
});

const btnRegister = $('#btn-register');

/** Add event listener to register button */
const registerEvent = () => {
    btnRegister.on('click', (e) => {
        e.preventDefault();
        register();
    });
};

/** Check if user input is valid
 * 
 * @param {string} name (min. 4 alphanumeric characters)
 * @param {string} pw (min. 8 alphanumeric characters)
 * @param {string} mail (min. 7 alphanumeric characters)
 * @returns 
 */
const checkInput = (name, pw, mail) => {
    
    if (name.length >= 4 && 
        regexAlphaNum.test(name) &&
        pw.length >= 8 &&
        regexAlphaNum.test(pw) &&
        mail.length >= 7) {
        return true;
    } else {
        return false;
    }
}

/** Register a new user
 *  1. Get user input
 *  2. Send request to server
 *  3. Show user notification
 */
const register = () => {

    let username = $('#form-register input[name="username"]').val().trim();
    $('#form-register input[name="username"]').val('');
    let password = $('#form-register input[name="password"]').val().trim();
    $('#form-register input[name="password"]').val('');
    let email = $('#form-register input[name="email"]').val().trim();
    $('#form-register input[name="email"]').val('');

    let validation = checkInput(username, password, email);

    if (validation) {
        $.ajax({
            url: SERVER_URL + '/register',
            method: 'POST',
            data: {
                username: username,
                password: password,
                email: email
            },
            xhrFields: {withCredentials: true},
            success: (response) => {
                if (response === 'User existiert schon.') {
                    showRegisterFail2();
                } else {
                    showRegisterSuccess();
                }
            },
            error: (xhr, status, errorText) => {
                console.log("HTTP-Fehler, Status: ", status, errorText);
                showServerError();
            }
        });
    } else {showRegisterFail();}
};

/** Show user notification (registration successfull) */
const showRegisterSuccess = () => {
    createModalBox($('#main-login'));
    createModalContent('Fast geschafft!', 
    `Dein Account wurde angelegt und ein E-Mail an die angegebene Adresse gesendet.
    Bitte klick auf den enthaltenen Link zur Bestätigung deiner E-Mail-Adresse.`);
};

/** Show user notification (registration failed) */
const showRegisterFail = () => {
    createModalBox($('#main-login'));
    createModalContent('Unzulässige Eingabe',
        'Bitte gib einen gültigen Benutzernamen (min. 4 alphanumerische Zeichen, ' +
        'keine Sonderzeichen) und ein gültiges Passwort (min. 8 alphanumerische Zeichen, ' + 
        'keine Sonderzeichen) ein.')
};

/** Show user notification (registration failed, username not available) */
const showRegisterFail2 = () => {
    createModalBox($('#main-login'));
    createModalContent('Benutzername nicht verfügbar', 'Dieser Benutzername existiert bereits. Bitte wähle einen anderen Usernamen.');
}

/** Initialization of event handler after DOMContentLoaded */
const init2 = () => {
    registerEvent();
};
