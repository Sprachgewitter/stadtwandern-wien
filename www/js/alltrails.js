myStorage = localStorage;
let allTrailsLocalStorage;

/** Request available data for all trails from server and
 *  store them in local storage
 */
const getTrails = () => {
    $.ajax({
        url: SERVER_URL + '/members/gettrails',
        method: 'GET',
        xhrFields: { withCredentials: true },
        /**
         * @param {object[]} response array of objects, each object contains details for one trail
         */
        success: (response) => {
            // Store trail details in local storage 
            localStorage.setItem('allTrailsLocalStorage', response);
            let allTrails = JSON.parse(response);
            showAllTrails(allTrails);
        },
        error: (xhr, status, errorText) => {
            console.log('HTTP-Fehler, Status: ', status, errorText);
        }
    });
};

// Request list of trails marked as 'done' by user
const getListTrailsDone = () => {
    let divs = document.querySelectorAll('div[id^="track-"]');
    $(divs).css('display', 'block');

    $.ajax({
        url: SERVER_URL + '/members/listtrailsdone',
        method: 'GET',
        xhrFields: { withCredentials: true },
        /**
         * @param {object[]} response list of trackIDs
         */
        success: (response) => {
            if (response.length >= 1) {
                let list = filter(response);
                let divs = document.querySelectorAll('div[id^="track-"]');
                let filteredDivs = [];
                [...divs].forEach(e => {
                    if (!list.includes(e.id.slice(6, e.id.length))) {
                        filteredDivs.push(e);
                    }
                });
                filteredDivs.forEach(e => $(e).css('display', 'none'));
            } else  {
                showNoTrailsDone();
            }
        },
        error: (xhr, status, errorText) => {
            console.log('HTTP-Fehler, Status: ', status, errorText);
        }
    });
};

// Request list of trails NOT marked as 'done' by user
const getListTrailsNew = () => {
    let divs = document.querySelectorAll('div[id^="track-"]');
    $(divs).css('display', 'block');

    $.ajax({
        url: SERVER_URL + '/members/listtrailsdone',
        method: 'GET',
        xhrFields: { withCredentials: true },
        success: (response) => {
            let list = filter(response);
            
            let filteredDivs = [];
            [...divs].forEach(e => {
                if (list.includes(e.id.slice(6, e.id.length))) {
                    filteredDivs.push(e);
                }
            });
            filteredDivs.forEach(e => $(e).css('display', 'none'));
        },
        error: (xhr, status, errorText) => {
            console.log('HTTP-Fehler, Status: ', status, errorText);
        }
    });
};

/** Shows user notification "no trails marked as done" */
const showNoTrailsDone = () => {
    createModalBox($('#alltrails'));
    createModalContent('Keine Wege vorhanden', 'Du hast noch keine Wanderwege aufgezeichnet oder als gemacht markiert.');
};

/** Create view for main page (alltrails.html)
 * 
 * @param {object[]} allTrails array of objects, each object contains details for one trail
 */
const showAllTrails = (allTrails) => {
    let container = $('#list-all-trails').addClass('container');
    let picNo = 0;

    allTrails.forEach((e) => {
        picNo++;
        let idNo = e.name.slice(15, e.name.indexOf(':'));
        let a = $('<a>').attr('href', `details.html?track=${idNo}&pic=${picNo}`).appendTo(container);
        let row = $('<div>').attr('id', 'track-' + idNo).addClass('row').appendTo(a);
        let col2 = $('<div>').addClass('col-2').appendTo(row);
        $('<img>').attr('src', `img/${picNo}.jpg`).appendTo(col2);
        let col4 = $('<div>').addClass('col-4').appendTo(row);
        $('<h2>').text(e.name.slice(0, e.name.indexOf(':'))).appendTo(col4);
        $('<h3>').text(e.name.slice((e.name.indexOf(':') + 1), e.name.length)).appendTo(col4);
        $('<p>').text(`${e.distance.replace('Kilometer', 'km')}, ca. ${e.duration.replace('Stunden', 'h')}`).appendTo(col4);
    });

    const btnShowOnlyDone = 
        $('<button>')
            .attr('type', 'button')
            .addClass('btn-large')
            .attr('id', 'btn-completed-trails')
            .text('Nach gemachten Wegen filtern')
            .appendTo(container);

    const btnShowOnlyNew = 
        $('<button>')
            .attr('type', 'button')
            .addClass('btn-large')
            .attr('id', 'btn-favorite-trails')
            .text('Nach nicht gemachten Wegen filtern')
            .appendTo(container);

    // Show only trails marked as 'done' (in main view)
    btnShowOnlyDone.on('click', (e) => {
        e.preventDefault();
        getListTrailsDone();
        $('#btn-completed-trails').addClass('hidden');
        $('#btn-favorite-trails').removeClass('hidden');
    });

    // Show only trails NOT marked as 'done' (in main view)
    btnShowOnlyNew.on('click', (e) => {
        e.preventDefault();
        getListTrailsNew();
        $('#btn-completed-trails').removeClass('hidden');
        $('#btn-favorite-trails').addClass('hidden');

    });
};

const filter = (list) => {
    let filteredList = stadtwanderwege.filter(element => list.includes(element));
    return filteredList;
}

getTrails();