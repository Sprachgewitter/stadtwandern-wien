/** Request tracked user trails from server */
const getMyTrails = () => {
    $.ajax({
        url: SERVER_URL + '/members/mytrails',
        method: 'GET',
        xhrFields: { withCredentials: true },
        success: (response) => {
            if (response === 'Keine Wanderwege gespeichert.') {
                showNoTrackedTrails();
            } else {
                listMyTrails(response);
            }
        },
        error: (status, errorText) => {
            console.log('HTTP-Fehler, Status:', status, errorText);
        }
    });
};

/** Assign correct picture to track
 * 
 * @param {string} trackID 
 * @returns path to picture
 */
const assignPic = (trackID) => {
    let path = `img/${stadtwanderwege.indexOf(trackID) + 1}.jpg`;
    return path;
}

/** Show list of tracked trails
 * 
 * @param {object[]} myTrails [{trackID}, {timestamp}, {coordinates}]
 */
const listMyTrails = (myTrails) => {
    myTrails.forEach(e => {

        // Only show trails that contain coordinates
        if (e.coordinates.length > 0) {
            let time = new Date(e.timestamp).toLocaleDateString('de-DE');
            let imgPath = assignPic(e.trackID);
            let row = $('<div>').addClass('row')
                .data('coordinates', e.coordinates)
                .data('trackID', e.trackID)
                .data('timestamp', time)
                .appendTo('#div-my-trails');
            let col2 = $('<div>').addClass('col-2').appendTo(row);
            $('<img>').attr('src', imgPath).appendTo(col2);
            let col4 = $('<div>').addClass('col-4').appendTo(row);
            $('<h2>').text(`Stadtwanderweg ${e.trackID}`).appendTo(col4);
            $('<p>').text(`Aufgezeichnet am ${time}`).appendTo(col4);

            /** Add event listener to show selected tracked trail on map */
            row.on('click', (e) => {
                e.preventDefault();
                $('#div-my-trails .row').removeClass('selected');
                let data = $(e.currentTarget).data();
                let coordinates = data.coordinates;
                let trackID = data.trackID;
                let timestamp = data.timestamp;
                $(e.currentTarget).addClass('selected');
                showMyTrail(coordinates, trackID, timestamp);
            });
        }
    });
};

let map;

/** Show selected tracked trail on map
 * 
 * @param {object[]} coordinates [lat, lng]
 * @param {string} trackID 
 * @param {number} timestamp unix timestamp
 */
const showMyTrail = (coordinates, trackID, timestamp) => {
    if (map != undefined) {
        map.remove();
    }
    if ($('#myTrailHeader') != undefined) {
        $('#myTrailHeader').remove();
    }
    let h1 = 'div.container>div.row:first-child';
    let row = $('<div>').addClass('row').attr('id', 'myTrailHeader').insertAfter(h1);
    let h2 = $('<h2>').text(`Stadtwanderweg ${trackID}`).appendTo(row);
    $('<p>').text(timestamp).insertAfter(h2);
    $(h1).css('display', 'none');
    $('#map-my-trail').css('display', 'block');

    // set view to first pair of coordinates (= starting point)
    map = L.map('map-my-trail').setView(coordinates[0], 13);

    let layer = L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 20,
        id: 'mapbox/streets-v11',
        tileSize: 512,
        zoomOffset: -1,
        accessToken: 'pk.eyJ1Ijoic3ByYWNoZ2V3aXR0ZXIiLCJhIjoiY2tzc25nbjhmMHk2cTJvbzRhczE4ajVwNiJ9.LJvsu1qJMUQskU1Rq-U_FQ'
    }).addTo(map);

    L.polyline(coordinates, { color: '#009900' }).addTo(map);

    $(document).scrollTop(0);
};

/** Show user notification (no tracked trails) */
const showNoTrackedTrails = () => {
    createModalBox($('#mytrails'));
    createModalContent('Keine Wege vorhanden', 'Du hast noch keine Wanderwege aufgezeichnet.');
}

getMyTrails();